# TASK_4

# Requirements for checking the task:

1. In terminal run the command: `npm install`
2. To check if all the tests are passed, run the command: `npm test`
3. To run smoke test use `npm run test:smoke`
4. To run prod environment use `npm run test:prod`

# What had done if compare with the task_3
1. Code refactoring is provided in the test.e2e.js file and in the 'pom' folder. 
2. Property files are used to store test data for at least two different environments. In the file named 'prod-test.e2e.js' was inserted prod properties from the 'data' folder. 
3. The code is more structured and readable.
4. Implemented screenshot with the date in the 'screenshots' folder if the test fails.
5. Provided suites for the smoke test. In general, finding the right base page and opening the calculator page seems to be a smoke test as it is crucial for further instructions in the task. 

# What should be done 

Use your completed Task 3 from the WebDriver module as a precondition for the current task, in which you need to develop a framework based on it, which will include the following features:

1. Page Object/Page Factory for page abstractions
2. Component objects of the required elements
3. Property files with test data for at least two different environments
4. Suites for smoke tests and other tests should be configured
5. If the test fails, a screenshot with the date and time is taken.

Please find the precondition discription below and provide the link to your solution in the comment field.
Precondition. Previous assignment (Task 3) from WebDriver module: 

Automate the following script: 

1. Open https://cloud.google.com/.
2. Click on the icon at the top of the portal page and enter "Google Cloud Platform Pricing Calculator" into the search field.
3. Perform the search.
4. Click "Google Cloud Platform Pricing Calculator" in the search results and go to the calculator page.
5. Click COMPUTE ENGINE at the top of the page.
6. Fill out the form with the following data:
   * Number of instances: 4
   * What are these instances for?: leave blank
   * Operating System / Software: Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS
   * Provisioning model: Regular
   * Machine Family: General purpose 
   * Series: N1 
   * Machine type: n1-standard-8 (vCPUs: 8, RAM: 30 GB)
   * Select “Add GPUs“
           * GPU type: NVIDIA Tesla V100
           * Number of GPUs: 1
   * Local SSD: 2x375 Gb
   * Datacenter location: Frankfurt (europe-west3)
   * Committed usage: 1 Year
Other options leave in the default state.
7. Click 'Add to Estimate'.
8. Check the price is calculated in the right section of the calculator. There is a line “Total Estimated Cost: USD ${amount} per 1 month” 
9. Click "Share" to see Total estimated cost.
10. Click "Open estimate summary" to see Cost Estimate Summary, will be opened in separate tab browser.
11. Verify that the 'Cost Estimate Summary' matches with filled values in Step 6. 