export const data = {

    searchInput: 'Google Cloud Platform Pricing Calculator',

    numberOfInstancesFieldName:'Number of Instances',
    systemFieldName: 'Operating System / Software',
    machineFieldName: 'Machine type',
    gpuModelFieldName: 'GPU Model',
    numberOfGpusFieldName: 'Number of GPUs',
    localSSDFieldName: 'Local SSD',
    regionFieldName: 'Region',
    discountOptionFieldName: 'Committed use discount options',

    numberOfInstancesValue: '12',
    systemValue: 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)',
    machineValue: 'n1-standard-4',
    gpuModelValue: 'NVIDIA Tesla T4',
    numberOfGpusValue: '2',
    localSSDValue: '6x375 GB',
    regionValue: 'Netherlands (europe-west4)',
    discountOptionValue: '1 year',

    expectedCost: '$6,516.15'
}