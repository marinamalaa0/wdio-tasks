class SearchComponent {

    get rootEl() {
        return $('.ipyBKf');
    }

    calculatorLink() {
        return this.rootEl.$('[href="https://cloud.google.com/products/calculator"]').click();
    }
}

export default SearchComponent;