class HeaderComponent {

    get rootEl () {
        return $('.TDbJKc');
    }

    searchBtn () {
        return this.rootEl.$('.p1o4Hf').click();
    }

    get formPaste () {
        return this.rootEl.$('.qdOxv-fmcmS-wGMbrd');
    }

    async inputSearchType(searchInput) {
        await this.formPaste.setValue(searchInput)
    }
    
    formBtn () {
        return this.rootEl.$('[jscontroller="MH0hJe"]').click();
    }
}

export default HeaderComponent;