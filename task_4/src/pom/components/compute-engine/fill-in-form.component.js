class FillInFormComponent {
    async openDropdown(dropdownName) {
        $(`//span[text()="${dropdownName}"]/ancestor::div[contains(@class, "O1htCb-H9tDt")]`).click();
        $('.VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c').waitForExist();
    } 

    selectOptionByText(text) {
        $(`//span[text()='${text}']/ancestor::li`).click();
    } 

    get numberOfInstances () {
        return $('.QiFlid');
    }

    get instancesOption () {
        return this.numberOfInstances.$('.qdOxv-fmcmS-wGMbrd');
    }

    inputNumberInstances(numbersOfInstances) {
        return this.instancesOption.setValue(numbersOfInstances)
    }

    get operatingSysnem() {
        return $('[data-field-type="106"]')
    }

    get systemOption() {
        return this.operatingSysnem.$('[aria-label="Operating System / Software"]')
    }

    get systemOp() {
        return this.operatingSysnem.$('.VfPpkd-rymPhb-ibnC6b-OWXEXe-SfQLQb-Woal0c-RWgCYc')

    } 

    async inputOperatingSystem(text) {
        await this.operatingSysnem.click();
        await this.systemOption.waitForDisplayed();
        this.systemOp.click();
        const option = $(`//option[text()='${text}']`);
        option.click(); 
    }

    addGPU() {
        return $('[data-field-type="114"]').$('.AsBIyb').click();
    }

    provisionModel(){
        return $('label.zT2df[for="regular"]').click();
    }

    commitedUsage(){
        return $('label.zT2df[for ="1-year"]').click();
    }
}

export default FillInFormComponent;