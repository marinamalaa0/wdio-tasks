class SideMenuComponent {
    get costEstimateBlock() {
        return $('.fbc2ib');
    }

    get costEstimate() {
        browser.waitUntil(() => {
            try {
                const costElement = this.costEstimateBlock.$('.gt0C8e.MyvX5d.D0aEmf');
                return costElement.isDisplayed();
            } catch (error) {
                return false;
            }
        }, {
            timeout: 5000, 
            timeoutMsg: 'Cost estimate element not found within 5 seconds',
            interval: 200 
        });
        
        return this.costEstimateBlock.$('.gt0C8e.MyvX5d.D0aEmf').getText();
    }

    async shareBtn() {
        $('[aria-label="Open Share Estimate dialog"]').click();
    }

    get summaryElement() {
        return $('a.tltOzc.MExMre.rP2xkc.jl2ntd');
    }

    async waitSummary() {
        await this.summaryElement.waitForDisplayed();
    }

    get linkSummary() {
        return this.summaryElement.getAttribute('href');
    }
}

export default SideMenuComponent;