class SideMenuComponent {

    get rootEl() { 
        return $('.uMSQA');
    }

    addEstimateBtn() {
        return this.rootEl.$('.jirROd').click();
    }

    async addEngine() {
        const engineBtn = "//h2[text() = 'Compute Engine']"
        await $(engineBtn).waitForDisplayed();
        await $(engineBtn).click();
    }

}

export default SideMenuComponent;