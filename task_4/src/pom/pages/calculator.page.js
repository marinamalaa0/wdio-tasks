import BasePage from "./base.page.js";

import SideMenuComponent from '../components/sidemenu.component.js';

class CalculatorPage extends BasePage {

    constructor() {
        super('/products/calculator');

        this.sideMenu = new SideMenuComponent()
    }
}

export default CalculatorPage;