import HeaderComponent from "../components/common/header.component.js";

class BasePage {

    constructor (url) {
        this.url = url;
        this.header = new HeaderComponent;
    }

    open () {
        return browser.url(this.url)
    }
}
export default BasePage;