import BasePage from "./base.page.js";

import FillInFormComponent from "../components/compute-engine/fill-in-form.component.js";
import SideMenuComponent from '../components/compute-engine/compute-engine-sidemenu.component.js'

class ComputerEnginePage extends BasePage {
    constructor() {
        super('/products/calculator?dl=CiQ1NGE1MTYzMC03ZjVmLTRmMzMtYmQxZi0wYzllNGFiZWZlZDgQCBokRTExREREQ0UtODVDMy00QURBLTlBQkQtM0FFRDk5QjM2MzYw');

        this.fillInForm = new FillInFormComponent()
        this.sideMenu = new SideMenuComponent();
    }
} 

export default ComputerEnginePage;