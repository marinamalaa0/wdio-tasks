import { assert } from "chai";

import GoogleCloudPage from '../../pom/pages/home.page.js';
import SearchPage from '../../pom/pages/search.page.js';
import CalculatorPage from '../../pom/pages/calculator.page.js';
import ComputeEnginePage from '../../pom/pages/compute-engine.page.js'
import SummaryPage from '../../pom/pages/summary.page.js';
import { data as currentProperty } from '../../data/local-compute-engine.js';

const gCloudPage = new GoogleCloudPage();
const searchPage = new SearchPage();
const calculatorPage = new CalculatorPage();
const computeEnginePage = new ComputeEnginePage();
const summaryPage = new SummaryPage();

describe('Google Cloud', () => {

    it('gCloud page @smoke', async () => {
        await browser.maximizeWindow()
        await gCloudPage.open();
        await gCloudPage.header.searchBtn();
        await gCloudPage.header.inputSearchType(currentProperty.searchInput);
        await gCloudPage.header.formBtn();
        await searchPage.gSearch.calculatorLink();

        await calculatorPage.sideMenu.addEstimateBtn();
        calculatorPage.sideMenu.addEngine();

        await computeEnginePage.fillInForm.inputNumberInstances(currentProperty.numberOfInstancesValue);

        computeEnginePage.fillInForm.inputOperatingSystem(currentProperty.systemValue);

        await computeEnginePage.fillInForm.provisionModel();
        
        await computeEnginePage.fillInForm.openDropdown(currentProperty.machineFieldName);
        computeEnginePage.fillInForm.selectOptionByText(currentProperty.machineValue);

        await computeEnginePage.fillInForm.addGPU();

        await computeEnginePage.fillInForm.openDropdown(currentProperty.gpuModelFieldName);
        computeEnginePage.fillInForm.selectOptionByText(currentProperty.gpuModelValue);

        await computeEnginePage.fillInForm.openDropdown(currentProperty.numberOfGpusFieldName);
        computeEnginePage.fillInForm.selectOptionByText(currentProperty.numberOfGpusValue);

        await computeEnginePage.fillInForm.openDropdown(currentProperty.localSSDFieldName);
        computeEnginePage.fillInForm.selectOptionByText(currentProperty.localSSDValue);

        await computeEnginePage.fillInForm.openDropdown(currentProperty.regionFieldName);
        computeEnginePage.fillInForm.selectOptionByText(currentProperty.regionValue);

        await computeEnginePage.fillInForm.commitedUsage();

        const cost = await computeEnginePage.sideMenu.costEstimate
        assert.isNotEmpty(cost, "Cost estimate text should not be empty");

        await computeEnginePage.sideMenu.shareBtn();
        computeEnginePage.sideMenu.waitSummary();

        const relativeURL = await computeEnginePage.sideMenu.linkSummary;

        const completeURL = `https://cloud.google.com/${relativeURL}`;
        const summaryPage = new SummaryPage(completeURL);
        
        await summaryPage.open();

        await summaryPage.summary.rootEl.waitForDisplayed();

        const actualNumberOfInstances = await summaryPage.summary.summaryResult(currentProperty.numberOfInstancesFieldName);
        assert.strictEqual(actualNumberOfInstances, currentProperty.numberOfInstancesValue);

        const actualOperatingSystem = await summaryPage.summary.summaryResult(currentProperty.systemFieldName);
        assert.strictEqual(actualOperatingSystem, currentProperty.systemValue);

        const actualMachineType = (await summaryPage.summary.summaryResult(currentProperty.machineFieldName)).split(',')[0];
        assert.strictEqual(actualMachineType, currentProperty.machineValue);

        const actualGpuModel = await summaryPage.summary.summaryResult(currentProperty.gpuModelFieldName);
        assert.strictEqual(actualGpuModel, currentProperty.gpuModelValue);

        const actualNumberOfGpus = await summaryPage.summary.summaryResult(currentProperty.numberOfGpusFieldName);
        assert.strictEqual(actualNumberOfGpus, currentProperty.numberOfGpusValue);

        const actualLocalSsd = await summaryPage.summary.summaryResult(currentProperty.localSSDFieldName);
        assert.strictEqual(actualLocalSsd, currentProperty.localSSDValue);

        const actualRegion = await summaryPage.summary.summaryResult(currentProperty.regionFieldName);
        assert.strictEqual(actualRegion, currentProperty.regionValue);

        const actualCommittedUse = await summaryPage.summary.summaryResult(currentProperty.discountOptionFieldName);
        assert.strictEqual(actualCommittedUse, currentProperty.discountOptionValue);
    })
})


