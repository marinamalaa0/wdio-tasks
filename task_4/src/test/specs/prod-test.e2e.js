import GoogleCloudPage from '../../pom/pages/home.page.js';
import SearchPage from '../../pom/pages/search.page.js';
import CalculatorPage from '../../pom/pages/calculator.page.js';
import ComputeEnginePage from '../../pom/pages/compute-engine.page.js'
import SummaryPage from '../../pom/pages/summary.page.js';
import { data as prodProperty } from '../../data/prod-compute-engine.js';

const gCloudPage = new GoogleCloudPage();
const searchPage = new SearchPage();
const calculatorPage = new CalculatorPage();
const computeEnginePage = new ComputeEnginePage();
const summaryPage = new SummaryPage();

describe('Google Cloud', () => {

    it('gCloud page @smoke', async () => {
        await browser.maximizeWindow()
        await gCloudPage.open();
        await gCloudPage.header.searchBtn();
        await gCloudPage.header.inputSearchType(prodProperty.searchInput);
        await gCloudPage.header.formBtn();
        await searchPage.gSearch.calculatorLink();
        
    })

    it('Calculator page @smoke', async () => {
        await calculatorPage.sideMenu.addEstimateBtn();
        calculatorPage.sideMenu.addEngine();
    })

    it('Fill in form', async () => {
        await computeEnginePage.numbersOfInstances.inputNumberInstances(prodProperty.numberOfInstancesValue);

        computeEnginePage.operatingSystem.inputOperatingSystem(prodProperty.systemValue);

        await computeEnginePage.buttonClick.provisionModel();
        
        await computeEnginePage.dropDownInput.openDropdown(prodProperty.machineFieldName);
        computeEnginePage.dropDownInput.selectOptionByText(prodProperty.machineValue);

        await computeEnginePage.toggleButton.addGPU();

        await computeEnginePage.dropDownInput.openDropdown(prodProperty.gpuModelFieldName);
        computeEnginePage.dropDownInput.selectOptionByText(prodProperty.gpuModelValue);

        await computeEnginePage.dropDownInput.openDropdown(prodProperty.numberOfGpusFieldName);
        computeEnginePage.dropDownInput.selectOptionByText(prodProperty.numberOfGpusValue);

        await computeEnginePage.dropDownInput.openDropdown(prodProperty.localSSDFieldName);
        computeEnginePage.dropDownInput.selectOptionByText(prodProperty.localSSDValue);

        await computeEnginePage.dropDownInput.openDropdown(prodProperty.regionFieldName);
        computeEnginePage.dropDownInput.selectOptionByText(prodProperty.regionValue);

        await computeEnginePage.buttonClick.commitedUsage(); 
    })

    it('SideMenu', async () => {
        await browser.pause(5000);
        const actualCost = await computeEnginePage.sideMenu.costEstimate;
        await expect(actualCost).toBe(prodProperty.expectedCost);

        await computeEnginePage.sideMenu.shareBtn();
        computeEnginePage.sideMenu.waitSummary();

        const relativeURL = await computeEnginePage.sideMenu.linkSummary;

        const completeURL = `https://cloud.google.com/${relativeURL}`;
        const summaryPage = new SummaryPage(completeURL);
        
        await summaryPage.open();
    })

    it('Verify Cost Estimate Summary matches filled values', async () => {
        await summaryPage.summary.rootEl.waitForDisplayed();

        const actualnumberOfInstances = await summaryPage.summary.summaryResult(prodProperty.numberOfInstancesFieldName);
        await expect(actualnumberOfInstances).toBe(prodProperty.numberOfInstancesValue);

        const actualOperatingSystem = await summaryPage.summary.summaryResult(prodProperty.systemFieldName);
        await expect(actualOperatingSystem).toBe(prodProperty.systemValue);

        const actualMachineType = (await summaryPage.summary.summaryResult(prodProperty.machineFieldName)).split(',')[0];
        await expect(actualMachineType).toBe(prodProperty.machineValue);

        const actualGpuModel = await summaryPage.summary.summaryResult(prodProperty.gpuModelFieldName);
        await expect(actualGpuModel).toBe(prodProperty.gpuModelValue);

        const actualNmbrsOfGpus = await summaryPage.summary.summaryResult(prodProperty.numberOfGpusFieldName);
        await expect(actualNmbrsOfGpus).toBe(prodProperty.numberOfGpusValue);

        const actualLocalSsd = await summaryPage.summary.summaryResult(prodProperty.localSSDFieldName);
        await expect(actualLocalSsd).toBe(prodProperty.localSSDValue);

        const actualRegion = await summaryPage.summary.summaryResult(prodProperty.regionFieldName);
        await expect(actualRegion).toBe(prodProperty.regionValue);

        const actualCommittedUse = await summaryPage.summary.summaryResult(prodProperty.discountOptionFieldName);
        await expect(actualCommittedUse).toBe(prodProperty.discountOptionValue);
    })
})


