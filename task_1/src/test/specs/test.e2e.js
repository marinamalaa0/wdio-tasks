import PastebinPage from '../../pom/pages/pastebin.page.js';

const pastebinPage = new PastebinPage();

describe('Pastebin page', () => {
    
    it('Add new paste', async () => {
        await pastebinPage.open();
        await pastebinPage.pasteBinHeader.addNewPasteBtn.click();

        await pastebinPage.textareaElement.rootEl.setValue('Hello from WebDriver');

        await pastebinPage.newPasteCreate.setExpiration('10 Minutes');
        await pastebinPage.newPasteCreate.postformName.setValue('helloweb');
        await pastebinPage.newPasteCreate.createBtn.click();
    })
})


