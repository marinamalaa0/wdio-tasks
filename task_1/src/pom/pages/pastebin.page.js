import BasePage from './base.page.js';

import HeaderComponent from './../components/newpaste/header.component.js';
import TextareaElement from './../components/newpaste/add-textarea.component.js';
import NewPasteCreate from './../components/newpaste/optional.component.js'

class PastebinPage extends BasePage{
    constructor () {
        super('');
        
        this.pasteBinHeader = new HeaderComponent();
        this.textareaElement = new TextareaElement();
        this.newPasteCreate = new NewPasteCreate();
    }
}
export default PastebinPage;