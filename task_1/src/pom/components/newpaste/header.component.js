class HeaderComponent {

    get rootEl () {
        return $('.header__container');
    }

    get addNewPasteBtn () {
        return this.rootEl.$('a.header__btn');
    }
}

export default HeaderComponent;