class NewPasteCreate {
    get rootEl() {
        return $('.post-form__left');
    }
    get expirationDropdown() {
        return this.rootEl.$('span#select2-postform-expiration-container');
    }
    async setExpiration(period) {
        await this.expirationDropdown.click();
        await $(`//li[text()='${period}']`).click();
    }
    get postformName() {
        return this.rootEl.$('#postform-name')
    }
    get createBtn() {
        return this.rootEl.$("//button[text() = 'Create New Paste']")
    }
} 

export default NewPasteCreate; 