# TASK_1

# Requirements for checking the task:

1. In terminal run the command: `npm install`
2. To check if all the tests are passed, run the command: `npm test`

# What should be done 

When performing a task, you must use the capabilities of WebdriverIO, a unit test framework (Mocha) and the Page Object concept.

Automate the following script: 

1. Open https://pastebin.com/ or a similar service in any browser.
2. Create 'New Paste' with the following attributes:
* Code: "Hello from WebDriver"
* Paste Expiration: "10 Minutes"
* Paste Name / Title: "helloweb"
