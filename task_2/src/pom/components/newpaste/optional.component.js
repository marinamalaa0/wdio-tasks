class RootComponent {
    get rootEl() {
        return $('.post-form__left');
}
}

class SyntaxElement extends RootComponent {
    get highlightingDropdown() {
        return this.rootEl.$('span#select2-postform-format-container')
    }

    async setSyntax(period) {
        await this.highlightingDropdown.click();
        await $(`//li[text()='${period}']`).click();
    }
}

class ExpirationElement extends RootComponent {
    get expirationDropdown() {
        return this.rootEl.$('span#select2-postform-expiration-container')
    }

    async setExpiration(period) {
        await this.expirationDropdown.click();
        await $(`//li[text()='${period}']`).click();
    }
}

class PasteTextElement extends RootComponent {
    get postformName() {
        return this.rootEl.$('#postform-name')
    }
}

class CreateNewPaste extends RootComponent {
    get createBtn() {
        return this.rootEl.$("//button[text() = 'Create New Paste']")
    }
}

export { ExpirationElement, PasteTextElement, CreateNewPaste, SyntaxElement }; 