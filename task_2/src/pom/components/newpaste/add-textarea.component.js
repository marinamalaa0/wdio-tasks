class TextAreaElement {
    get postformText() {
        return $('#postform-text');
    }
    
    get rootEl() {
        return $('.post-view')
    }
    get readyTextArea() {
        return this.rootEl.$('ol.bash');
    }
}

export default TextAreaElement;