class BasePage {

    constructor (url) {
        this.url = url;
    }

    open () {
        return browser.url(this.url)
    }

    getTitle () {
        return browser.getTitle();
    }
}
export default BasePage;