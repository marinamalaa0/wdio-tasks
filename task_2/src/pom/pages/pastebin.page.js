import BasePage from './base.page.js';

import HeaderComponent from '../components/newpaste/header.component.js';
import TextAreaElement from '../components/newpaste/add-textarea.component.js'
import { ExpirationElement, PasteTextElement, CreateNewPaste, SyntaxElement } from '../components/newpaste/optional.component.js';
import { ReadyPaste } from '../components/newpaste/ready-paste.component.js';


class PastebinPage extends BasePage{

    constructor () {
        super('');
        
        this.header = new HeaderComponent();
        this.textAreaElement = new TextAreaElement();
        this.syntaxElement = new SyntaxElement();
        this.expirationElement = new ExpirationElement();
        this.pasteTextElement = new PasteTextElement();
        this.createNewPaste = new CreateNewPaste();
        this.readyPaste = new ReadyPaste();
    }
}

export default PastebinPage;