import PastebinPage from '../../pom/pages/pastebin.page.js';

const pastebinPage = new PastebinPage();
const titlePaste = 'how to gain dominance among developers';
const textAreaContent = 'git config --global user.name "New Sheriff in Town"\ngit reset $ (git commit-tree HEAD ^ {tree} -m "Legacy code")\ngit push origin master --force\n';
const syntaxMatchesValue = 'Bash';
const expirationValue = '10 Minutes';

describe('Pastebin page', () => {

    it('Add new paste', async () => {
        await pastebinPage.open();
        await pastebinPage.header.addNewPasteBtn.click();

        await pastebinPage.textAreaElement.postformText.setValue(textAreaContent);
        await pastebinPage.syntaxElement.setSyntax(syntaxMatchesValue);
        await pastebinPage.expirationElement.setExpiration(expirationValue);
        await pastebinPage.pasteTextElement.postformName.setValue(titlePaste);

        await pastebinPage.createNewPaste.createBtn.click();
    })

    it('Check if browser page title matches Paste Name / Title', async () => {
        const titleMatches = await pastebinPage.getTitle();
        expect(titleMatches).toBe(titlePaste.concat(' - Pastebin.com'));
    })

    it('Syntax is suspended for Bash', async () => {
        await expect(pastebinPage.readyPaste.syntaxMatches.toEqual(syntaxMatchesValue)); 
    });

    it('Code matches the one entered', async () => {    
        const expectedText = await pastebinPage.textAreaElement.readyTextArea.getText();
        await expect(expectedText.trim()).toBe(textAreaContent.trim());
    }); 
})


