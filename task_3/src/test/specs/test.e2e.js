import GoogleCloudPage from '../../pom/pages/home.page.js';
import SearchPage from '../../pom/pages/search.page.js';
import CalculatorPage from '../../pom/pages/calculator.page.js';
import ComputeEnginePage from '../../pom/pages/compute-engine.page.js'
import SummaryPage from '../../pom/pages/summary.page.js';

const gCloudPage = new GoogleCloudPage();
const searchPage = new SearchPage();
const calculatorPage = new CalculatorPage();
const computeEnginePage = new ComputeEnginePage();
const summaryPage = new SummaryPage();

const numbersOfInstances = '4';
const systemValue = 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)';
const machineValue = 'n1-standard-8';
const nmbrOfGpus = '1';
const localSSDValue = '2x375 GB';

describe('Google Cloud', () => {

    it('gCloud hage', async () => {
        await browser.maximizeWindow()
        await gCloudPage.open();
        await gCloudPage.header.searchBtn.click();
        await gCloudPage.header.formPaste.setValue('Google Cloud Platform Pricing Calculator');
        await gCloudPage.header.formBtn.click();
        await searchPage.gSearch.calculatorLink.click();
        
    })

    it('Calculator page', async () => {
        await calculatorPage.sideMenu.addEstimateBtn.click();
        await calculatorPage.sideMenu.addEngine.waitForDisplayed();
        await calculatorPage.sideMenu.addEngine.click();
    })

    it('Fill in form', async () => {
        await computeEnginePage.numbersOfInstances.instancesOption.setValue(numbersOfInstances);

        await computeEnginePage.operatingSystem.rootEl.click();
        await computeEnginePage.operatingSystem.systemOption.waitForDisplayed();
        computeEnginePage.operatingSystem.selectOptionByText(systemValue);

        await computeEnginePage.provModel.provModelOption.click();
        
        await computeEnginePage.machineType.rootEl.click();
        await computeEnginePage.machineType.typeOption.waitForDisplayed();
        computeEnginePage.machineType.selectOptionByText(machineValue);

        await computeEnginePage.addGpu.addBtn.click();

        await computeEnginePage.gpuModel.rootEl.click();
        await computeEnginePage.gpuModel.gpuOption.waitForDisplayed();
        computeEnginePage.gpuModel.selectByAttribute('data-value', 'nvidia-tesla-v100');
        
        await computeEnginePage.numberOfGpus.rootEl.click();
        await computeEnginePage.numberOfGpus.nmbrOption.waitForDisplayed();
        computeEnginePage.numberOfGpus.selectByAttribute('data-value', nmbrOfGpus);

        await computeEnginePage.localSSD.rootEl.click();
        await computeEnginePage.localSSD.localOption.waitForDisplayed();
        computeEnginePage.localSSD.selectOptionByText(localSSDValue);
        
        await computeEnginePage.region.rootEl.click();
        await computeEnginePage.region.regionOption.waitForDisplayed();
        computeEnginePage.region.selectByAttribute('data-value', 'europe-west4');

        await computeEnginePage.committedUsage.useOption.click(); 
    })

    it('SideMenu', async () => {
        await browser.pause(5000)
        const actualCost = await computeEnginePage.sideMenu.costEstimate.getText();
        const expectedCost = '$5,628.90';
        await expect(actualCost).toBe(expectedCost);

        await computeEnginePage.sideMenu.shareBtn.click();
        await computeEnginePage.sideMenu.openSummary.waitForDisplayed();

        const relativeURL = await computeEnginePage.sideMenu.openSummary.getAttribute('href');

        const completeURL = `https://cloud.google.com/${relativeURL}`;
        const summaryPage = new SummaryPage(completeURL);
        
        await summaryPage.open();
    })

    it('Verify Cost Estimate Summary matches filled values', async () => {
        await summaryPage.summary.rootEl.waitForDisplayed();

        const actualnumberOfInstances = await summaryPage.summary.summaryResult('Number of Instances').getText();
        await expect(actualnumberOfInstances).toBe(numbersOfInstances);

        const actualOperatingSystem = await summaryPage.summary.summaryResult('Operating System / Software').getText();
        await expect(actualOperatingSystem).toBe(systemValue);

        const actualMachineType = (await summaryPage.summary.summaryResult('Machine type').getText()).split(',')[0];
        await expect(actualMachineType).toBe(machineValue);

        const actualGpuModel = await summaryPage.summary.summaryResult('GPU Model').getText();
        await expect(actualGpuModel).toBe('NVIDIA Tesla V100');

        const actualNmbrsOfGpus = await summaryPage.summary.summaryResult('Number of GPUs').getText();
        await expect(actualNmbrsOfGpus).toBe(nmbrOfGpus);

        const actualLocalSsd = await summaryPage.summary.summaryResult('Local SSD').getText();
        await expect(actualLocalSsd).toBe(localSSDValue);

        const actualRegion = await summaryPage.summary.summaryResult('Region').getText();
        await expect(actualRegion).toBe('Netherlands (europe-west4)');

        const actualCommittedUse = await summaryPage.summary.summaryResult('Committed use discount options').getText();
        await expect(actualCommittedUse).toBe('1 year');
    })
})


