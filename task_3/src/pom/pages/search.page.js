import BasePage from "./base.page.js"

import SearchComponent from '../components/search.component.js';

class SearchPage extends BasePage{

    constructor() {
        super('/search?q=Google%20Cloud%20Platform%20Pricing%20Calculator');

        this.gSearch = new SearchComponent();
    }
}

export default SearchPage;