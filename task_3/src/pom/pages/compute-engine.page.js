import BasePage from "./base.page.js";

import { NumbersOfInstances, OperatingSystem, ProvModel, MachineType, AddGpu, GPUModel, CommittedUsage, NumberOfGpus, LocalSSD, Region } from "../components/compute-engine/fill-in-form.component.js";

import SideMenuComponent from '../components/compute-engine/compute-engine-sidemenu.component.js'

class ComputeEnginePage extends BasePage {
    constructor() {
        super('/products/calculator?dl=CiQ1NGE1MTYzMC03ZjVmLTRmMzMtYmQxZi0wYzllNGFiZWZlZDgQCBokRTExREREQ0UtODVDMy00QURBLTlBQkQtM0FFRDk5QjM2MzYw');

        this.numbersOfInstances = new NumbersOfInstances();
        this.operatingSystem = new OperatingSystem();
        this.provModel = new ProvModel();
        this.machineType = new MachineType();
        this.addGpu = new AddGpu();
        this.gpuModel = new GPUModel();
        this.numberOfGpus = new NumberOfGpus();
        this.localSSD = new LocalSSD();
        this.region = new Region();
        this.committedUsage = new CommittedUsage();

        this.sideMenu = new SideMenuComponent();
    }
}

export default ComputeEnginePage;