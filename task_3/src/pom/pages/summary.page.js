import BasePage from "./base.page.js";
import Summary from "../components/summary.component.js";

class SummaryPage extends BasePage {
    constructor(url) {
        super(url);
        this.summary = new Summary();
    }
}

export default SummaryPage;
