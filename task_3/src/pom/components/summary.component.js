class Summary {
    get rootEl() {
        return $('.qBohdf.AlLELb')
    }

    summaryResult (text) {
        return this.rootEl.$(`//span[text()='${text}']/ancestor::span[contains(@class, "g5Ano")]/span[@class="Kfvdz"]`);
    }

}

export default Summary;