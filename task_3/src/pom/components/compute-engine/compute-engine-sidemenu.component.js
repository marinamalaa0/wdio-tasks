class SideMenuComponent {
    get rootEl() {
        return $('.fbc2ib');
    }

    get costEstimate() {
        return this.rootEl.$('.gt0C8e.MyvX5d.D0aEmf');
    }

    get shareBtn() {
        return $('[aria-label="Open Share Estimate dialog"]')
    }

    get openSummary() {
        return $('a.tltOzc.MExMre.rP2xkc.jl2ntd');
    }
}

export default SideMenuComponent;