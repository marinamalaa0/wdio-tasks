class NumbersOfInstances {

    get rootEl () {
        return $('.QiFlid');
    }

    get instancesOption () {
        return this.rootEl.$('.qdOxv-fmcmS-wGMbrd');
    }
}

class OperatingSystem {

    get rootEl() {
        return $('[data-field-type="106"]')
    }

    get systemOption() {
        return this.rootEl.$('[aria-label="Operating System / Software"]')
    }

    get systemOp() {
        return this.rootEl.$('.VfPpkd-rymPhb-ibnC6b-OWXEXe-SfQLQb-Woal0c-RWgCYc')

    } 

    selectOptionByText(text) {
        this.systemOp.click(); // Click the dropdown to open it
        const option = $(`//option[text()='${text}']`);
        option.click(); // Click the option
    }
}

class ProvModel {

    get rootEl() {
        return $('[data-field-type="107"]')
    }

    get provModelOption() {
        return this.rootEl.$('label.zT2df[for="regular"]');
    }
}

class MachineType {

    get rootEl() {
        return $('//span[text()="Machine type"]/ancestor::div[contains(@class, "O1htCb-H9tDt")]');
    }

    get typeOption() {
        return $('.VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c');
    }

    selectOptionByText(text) {
        const option = $(`//span[text()='${text}']/ancestor::li`);
        option.click(); 
    }


    selectByAttribute(attribute, value) {
        this.typeOption.click(); // Click the dropdown to open it
        const option = $(`[${attribute}='${value}']`);
        option.click(); // Click the option
    } 
}

class AddGpu {
    get rootEl() {
        return $('[data-field-type="114"]')
    }

    get addBtn() {
        return this.rootEl.$('.AsBIyb')
    }

}

class CommittedUsage {
    get rootEl() {
        return $('[data-field-type="116"]');
    }

    get useOption() {
        return this.rootEl.$('label.zT2df[for ="1-year"]')
    }
}

class GPUModel {
    get rootEl() {
        return $('//span[text()="GPU Model"]/ancestor::div[contains(@class, "VfPpkd-TkwUic")]')
    }

    get gpuOption() {
        return $('.VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c')
    }

    selectByAttribute(attribute, value) {
        this.gpuOption.click(); // Click the dropdown to open it
        const option = $(`[${attribute}='${value}']`);
        option.click(); // Click the option
    } 

}

class NumberOfGpus {
    get rootEl() {
        return $('//span[text()="Number of GPUs"]/ancestor::div[contains(@class, "VfPpkd-TkwUic")]');
    }

    get nmbrOption() {
        return $('.VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c')
    }

     selectByAttribute(attribute, value) {
        this.nmbrOption.click(); // Click the dropdown to open it
        const option = $(`[${attribute}='${value}']`);
        option.click(); // Click the option
    } 
}

class LocalSSD {
    get rootEl() {
        return $('//span[text()="Local SSD"]/ancestor::div[contains(@class, "VfPpkd-TkwUic")]');
    }

    get localOption() {
        return $('.VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c');
    }

    selectOptionByText(text) {
        const option = $(`//span[text()='${text}']/ancestor::li`);
        option.click(); 
    }


    selectByAttribute(attribute, value) {
        this.localOption.click(); // Click the dropdown to open it
        const option = $(`[${attribute}='${value}']`);
        option.click(); // Click the option
    }
}

class Region {
    get rootEl() {
        return $('//span[text()="Region"]/ancestor::div[contains(@class, "VfPpkd-TkwUic")]')
    }

    get regionOption() {
        return $('.VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c')
    }

    selectByAttribute(attribute, value) {
        this.regionOption.click(); // Click the dropdown to open it
        const option = $(`[${attribute}='${value}']`);
        option.click(); // Click the option
    } 

}





export { NumbersOfInstances, OperatingSystem, ProvModel, MachineType, AddGpu, CommittedUsage, GPUModel, NumberOfGpus, LocalSSD, Region };