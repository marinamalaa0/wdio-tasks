class HeaderComponent {

    get rootEl () {
        return $('.TDbJKc');
    }

    get searchBtn () {
        return this.rootEl.$('.p1o4Hf');
    }

    get formPaste () {
        return this.rootEl.$('.qdOxv-fmcmS-wGMbrd');
    }
    
    get formBtn () {
        return this.rootEl.$('[jscontroller="MH0hJe"]');
    }
}

export default HeaderComponent;