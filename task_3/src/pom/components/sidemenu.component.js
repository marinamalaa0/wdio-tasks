class SideMenuComponent {

    get rootEl() { 
        return $('.uMSQA');
    }

    get addEstimateBtn() {
        return this.rootEl.$('.jirROd');
    }

    get addEngine() {
        return $("//h2[text() = 'Compute Engine']");
    }

}

export default SideMenuComponent;