class SearchComponent {

    get rootEl() {
        return $('.ipyBKf');
    }

    get calculatorLink() {
        return this.rootEl.$('[href="https://cloud.google.com/products/calculator"]');
    }
}

export default SearchComponent;